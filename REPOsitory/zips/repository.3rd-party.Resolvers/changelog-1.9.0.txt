Version 1.9.0
 # 1st.Matrix [*todo*]

Version 1.0.6
 # -TVA (SMU with forced indigo)
 # +Gujal's *new* repository.resolveurl (SMR)

Version 1.0.5
 # -Eggman
 # +OpenScrapers (A4K)

Version 1.0.4
 # -Civitas

Version 1.0.3
 # -LambdaScrapersRepo
 # +Civitas Civitas Scrapers Module
 # +EggMan  EggScrapers Module

Version 1.0.2
 # LambdaScrapersRepo moved! url fix

Version 1.0.1
 # LambdaScrapersRepo zipped url fix

Version 1.0.0
 # initial release
