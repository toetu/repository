Version 1.9.1
 # 1st.Matrix [Py2to3]

Version 1.9.0
 # 1st.Matrix [*todo*]

Version 1.8.6.000
 # Matrix 'make-over' prep.

Version 1.8.3.000
 # 1st. Leia 'make-over'...

Version 1.6.1
 # MaintenanceRelease
 + "Bonus" Dependencies VoOdoO

Version 1.6.0
 # 3rd.Party Add-ons (mirrors)

Version 1.5.0
 # 3rd.Party KiDS (add-on mirrors)

Version 1.4.0
 # house cleaning vOoDoO

Version 1.3.0
 # Code Cosmetics VoOdOo

Version 1.2.0
 # 3rd.Party REPOsitories (mirrors)

Version 1.1.1
 # Code Cosmetics

Version 1.1.0
 # Subs added

Version 1.0.0
 # initial release
